# Spike Report

# AI2-advanced navigation

## Introduction

This spike aims to improve apron the AI1 spike by implementing more sophisticated movement such as the ue4 avoidance system. This will be achieved by delivering on the deliverables outlined In the spike plan

## Goals

1. To deliver on the spike deliverables as per the spike plan.
2. To expand on the understanding gained on in the AI spike, such that the specific areas include&quot;
  1. Nav mesh proxy actors
  2. Actor avoidance
3. To stay within the scope of this spike by following the spike plan.

## Personnel

| Primary – Matthew Dawkins | Secondary – N/A |
| --- | --- |

## Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

- Unreal Engine Nav Proxy Documentation: [https://docs.unrealengine.com/en-US/Resources/ContentExamples/NavMesh/1\_2/index.html](https://docs.unrealengine.com/en-US/Resources/ContentExamples/NavMesh/1_2/index.html)
- In depth nav proxy tutorial [https://www.vikram.codes/blog/ai/02-nav-modifiers-links](https://www.vikram.codes/blog/ai/02-nav-modifiers-links)
- Ai character avoidance [https://wiki.unrealengine.com/Unreal\_Engine\_AI\_Tutorial\_-\_2\_-\_Avoidance](https://wiki.unrealengine.com/Unreal_Engine_AI_Tutorial_-_2_-_Avoidance)

## Tasks undertaken

Overall there were two main tasks in this spike, firstly implementing the avoidance system and then adding the nav proxies.

**Avoidance System**

_In the aicontroler cpp file change the constructor to the following_

![](reportImages\1.png)

**NavMesh Proxies**

_For information on how to set up and modify the proxies see the Vikram code tutorials however the base process is as follows._

_Identify an area where the ai will need to jump or navigate an obstacle_

![](reportImages\2.png)

_Then select the nav link proxy_

![](reportImages\3.png)

_Finally set the parameters of the proxy as per what is needed, see tutorial for more._



## What we found out

The majority of the deliverable were relatively straight forward in this spike; however knowledge was gained when experimenting with the nav proxies. After completing the tutorial and playing around some considerably complex paths could be created. Some examples include:

- Jumping over gaps
- Jumping down from high places
- Traversing obstacles

In addition, the implantation of avoidance was considerably easy and painless.

##  Open Issues/risks

Some of the current issues of this spike include

- Limitations of the ai character not matching the nav proxies (for example a proxy telling the controller it can jump to a high place but the movement component not being able to)
- In large scale crowed the avoidance system is slow and unrealistic

## Recommendations

It is recommended that the following additional spikes be conducted

- Improved avoidance and crowed movement
- Linking the ai to the nav proxies
- Procedurally generating proxies as the ai moves along