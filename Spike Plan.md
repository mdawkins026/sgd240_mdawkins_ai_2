# Spike Plan

# AI 2 – Advanced Navigation

## Context

Now we&#39;ve got some basic Navigation systems going, it&#39;s time to make it work in C++!

We&#39;re also going to have a look at making our AI use an Avoidance system to walk around each other.

## Grade Level

Pass

## Gap

1. Tech: How can we get the Unreal Engine NavMesh working with C++?
2. Tech: Making Unreal Engine NavMesh allow for more movement types
3. Knowledge: How can we make our AI do more than just &quot;walk&quot;?
4. Knowledge: What systems for Avoidance does Unreal Engine offer?
5. Skill: Creating [autonomous agents](https://en.wikipedia.org/wiki/Autonomous_agent) that can avoid each other!

## Goals/Deliverables

- The Spike Report should answer each of the Gap questions

Building on Spike AI-1, add or replace the following:

- Replace your previous Blueprint Character and AIController with C++ versions that do the same thing.
- Update your level to include physical obstacles and Nav-Link Proxy actors between Waypoints, which your AI will use if possible.
  - At least one Nav-Link should be unidirectional (jumping off a cliff, etc.)
  - At least one Nav-Link should be bi-directional, using a custom movement mode (jumping over a gap).
- Create paths which cause the agents to run into each other, and ensure that they have Avoidance such that they don&#39;t block each other.
  - The easiest way to do this, is to have one AI follow a path forward, and another follow the same path backwards.

## Dates

| Planned start date: | Week 7 |
| --- | --- |
| Deadline: | Week 8 |

## Planning Notes

1. Read the Epic Games head of AI [tutorial on creating custom movement modes](https://wiki.unrealengine.com/Unreal_Engine_AI_Tutorial_-_1_-_Making_AI_Jump_as_a_Part_of_Path_Following) and [tutorial on using avoidance](https://wiki.unrealengine.com/Unreal_Engine_AI_Tutorial_-_2_-_Avoidance)
  1. Note: the tutorials are rather old, although the systems discussed have not changed very much, some of the C++ make be slightly outdated.
  2. If you find any elements which are outdated and do not work in UE4.15, ensure that you note them in your report (and perhaps you can edit the wiki article)!